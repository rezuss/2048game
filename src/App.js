import React, { useEffect, useState } from 'react';
import cloneDeep from 'lodash.clonedeep';

function App() {
	// Keycodes
	const UP_ARROW = 38;
	const DOWN_ARROW = 40;
	const LEFT_ARROW = 37;
	const RIGHT_ARROW = 39;

	// Main grid
	const [data, setData] = useState([
		[0, 0, 0, 0],
		[0, 0, 0, 0],
		[0, 0, 0, 0],
		[0, 0, 0, 0]
	]);

	// Win status
	const [win, setWin] = useState(false);

	// Game over status
	const [gameOver, setGameOver] = useState(false);

	// Best block
	const [bestBlock, setBestBlock] = useState(2);

	// Initialize game
	const initialize = () => {
		let newGrid = cloneDeep(data);
		newItem(newGrid);
		newItem(newGrid);
		setData(newGrid);
		checkBestBlock(newGrid);
	};

	// Create new item in the grid
	const newItem = (newGrid) => {
		let added = false;
		let gridFull = false;
		let attempts = 0;

		while (!added) {
			if (gridFull) {
				break;
			}

			let x = Math.floor(Math.random() * 4);
			let y = Math.floor(Math.random() * 4);
			attempts++;

			if (newGrid[x][y] === 0) {
				newGrid[x][y] = Math.random() > 0.5 ? 2 : 4;
				added = true;
			}

			if (attempts > 50) {
				gridFull = true;
			}
		}
	};

	// Swipe up
	const swipeUp = (dummy) => {
		let newGrid = cloneDeep(data);
		let oldGrid = data;

		for (let i = 0; i < 4; i++) {
			let slow = 0;
			let fast = 1;

			while (slow < 4) {
				if (fast === 4) {
					fast = slow + 1;
					slow++;
					continue;
				}

				if (newGrid[slow][i] === 0 && newGrid[fast][i] === 0) {
					fast++;
				} else if (newGrid[slow][i] === 0 && newGrid[fast][i] !== 0) {
					newGrid[slow][i] = newGrid[fast][i];
					newGrid[fast][i] = 0;
					fast++;
				} else if (newGrid[slow][i] !== 0 && newGrid[fast][i] === 0) {
					fast++;
				} else if (newGrid[slow][i] !== 0 && newGrid[fast][i] !== 0) {
					if (newGrid[slow][i] === newGrid[fast][i]) {
						newGrid[slow][i] = newGrid[slow][i] + newGrid[fast][i];
						newGrid[fast][i] = 0;
						fast = slow + 1;
						slow++;
					} else {
						slow++;
						fast = slow + 1;
					}
				}
			}
		}

		if (JSON.stringify(oldGrid) !== JSON.stringify(newGrid)) {
			newItem(newGrid);
		}

		checkBestBlock(newGrid);

		if (dummy) {
			return newGrid;
		} else {
			setData(newGrid);
		}
	};

	// Swipe down
	const swipeDown = (dummy) => {
		let newGrid = cloneDeep(data);
		let oldGrid = data;

		for (let i = 3; i >= 0; i--) {
			let slow = newGrid.length - 1;
			let fast = slow - 1;

			while (slow > 0) {
				if (fast === -1) {
					fast = slow - 1;
					slow--;
					continue;
				}

				if (newGrid[slow][i] === 0 && newGrid[fast][i] === 0) {
					fast--;
				} else if (newGrid[slow][i] === 0 && newGrid[fast][i] !== 0) {
					newGrid[slow][i] = newGrid[fast][i];
					newGrid[fast][i] = 0;
					fast--;
				} else if (newGrid[slow][i] !== 0 && newGrid[fast][i] === 0) {
					fast--;
				} else if (newGrid[slow][i] !== 0 && newGrid[fast][i] !== 0) {
					if (newGrid[slow][i] === newGrid[fast][i]) {
						newGrid[slow][i] = newGrid[slow][i] + newGrid[fast][i];
						newGrid[fast][i] = 0;
						fast = slow - 1;
						slow--;
					} else {
						slow--;
						fast = slow - 1;
					}
				}
			}
		}

		if (JSON.stringify(newGrid) !== JSON.stringify(oldGrid)) {
			newItem(newGrid);
		}

		checkBestBlock(newGrid);

		if (dummy) {
			return newGrid;
		} else {
			setData(newGrid);
		}
	};

	// Swipe left
	const swipeLeft = (dummy) => {
		let oldGrid = data;
		let newGrid = cloneDeep(data);

		for (let i = 0; i < 4; i++) {
			let b = newGrid[i];
			let slow = 0;
			let fast = 1;

			while (slow < 4) {
				if (fast === 4) {
					fast = slow + 1;
					slow++;
					continue;
				}

				if (b[slow] === 0 && b[fast] === 0) {
					fast++;
				} else if (b[slow] === 0 && b[fast] !== 0) {
					b[slow] = b[fast];
					b[fast] = 0;
					fast++;
				} else if (b[slow] !== 0 && b[fast] === 0) {
					fast++;
				} else if (b[slow] !== 0 && b[fast] !== 0) {
					if (b[slow] === b[fast]) {
						b[slow] = b[slow] + b[fast];
						b[fast] = 0;
						fast = slow + 1;
						slow++;
					} else {
						slow++;
						fast = slow + 1;
					}
				}
			}
		}

		if (JSON.stringify(oldGrid) !== JSON.stringify(newGrid)) {
			newItem(newGrid);
		}

		checkBestBlock(newGrid);

		if (dummy) {
			return newGrid;
		} else {
			setData(newGrid);
		}
	};

	// Swipe right
	const swipeRight = (dummy) => {
		let oldGrid = data;
		let newGrid = cloneDeep(data);

		for (let i = 3; i >= 0; i--) {
			let b = newGrid[i];
			let slow = b.length - 1;
			let fast = slow - 1;

			while (slow > 0) {
				if (fast === -1) {
					fast = slow - 1;
					slow--;
					continue;
				}

				if (b[slow] === 0 && b[fast] === 0) {
					fast--;
				} else if (b[slow] === 0 && b[fast] !== 0) {
					b[slow] = b[fast];
					b[fast] = 0;
					fast--;
				} else if (b[slow] !== 0 && b[fast] === 0) {
					fast--;
				} else if (b[slow] !== 0 && b[fast] !== 0) {
					if (b[slow] === b[fast]) {
						b[slow] = b[slow] + b[fast];
						b[fast] = 0;
						fast = slow - 1;
						slow--;
					} else {
						slow--;
						fast = slow - 1;
					}
				}
			}
		}

		if (JSON.stringify(newGrid) !== JSON.stringify(oldGrid)) {
			newItem(newGrid);
		}

		checkBestBlock(newGrid);

		if (dummy) {
			return newGrid;
		} else {
			setData(newGrid);
		}
	};

	// Check what is the best block and win
	const checkBestBlock = (grid) => {
		var max = 0;

		for (let row of grid) {
			for (let col of row) {
				if (col > max) max = col;
			}
		}

		if (max > bestBlock) {
			setBestBlock(max);
		}

		if (max === 2048) {
			setWin(true);
		}
	};

	// Check if game ended
	const checkIfGameOver = () => {
		let gameOver = true;
		let checkUp = swipeUp(true);
		let checkDown = swipeDown(true);
		let checkLeft = swipeLeft(true);
		let checkRight = swipeRight(true);

		if (JSON.stringify(data) !== JSON.stringify(checkUp)) {
			gameOver = false;
		}

		if (JSON.stringify(data) !== JSON.stringify(checkDown)) {
			gameOver = false;
		}

		if (JSON.stringify(data) !== JSON.stringify(checkLeft)) {
			gameOver = false;
		}

		if (JSON.stringify(data) !== JSON.stringify(checkRight)) {
			gameOver = false;
		}

		if (gameOver) {
			localStorage.setItem('score', bestBlock);
		}

		return gameOver;
	};

	// Reset game
	const reset = () => {
		setGameOver(false);
		setWin(false);
		const newGrid = [
			[0, 0, 0, 0],
			[0, 0, 0, 0],
			[0, 0, 0, 0],
			[0, 0, 0, 0]
		];
		newItem(newGrid);
		newItem(newGrid);
		setData(newGrid);
	};

	// Handle keydown
	const handleKeyDown = (event) => {
		if (gameOver || win) {
			return;
		}

		switch (event.keyCode) {
			case UP_ARROW:
				swipeUp();
				break;
			case DOWN_ARROW:
				swipeDown();
				break;
			case LEFT_ARROW:
				swipeLeft();
				break;
			case RIGHT_ARROW:
				swipeRight();
				break;
			default:
				break;
		}

		let gameEnded = checkIfGameOver();
		if (gameEnded) {
			setGameOver(true);
		}
	};

	// Keydown event listener
	const useEvent = (event, handler, passive = false) => {
		useEffect(() => {
			window.addEventListener(event, handler, passive);

			return function cleanup() {
				window.removeEventListener(event, handler);
			};
		});
	};

	useEvent('keydown', handleKeyDown);

	useEffect(() => {
		initialize();

		// must be empty array because while loop never stops
		// eslint-disable-next-line
	}, []);

	return (
		<div>
			<div className='header'>
				<button className='newgame' onClick={reset}>New Game</button>
				<div className='bestblock'>
					<div className='text'>Best block:</div>
					<div className='score' style={bestBlock === 2048 ? { color: 'darkred' } : {}}>{bestBlock}</div>
				</div>
				<div className='title'>2048</div>
			</div>
			<div className='container'>
				{gameOver && <div className='tryagain'>
					<div className='tryagainText'>Game Over</div>
					<button className='tryagainButton' onClick={reset}>Try again</button>
				</div>}
				{win && <div className='win'>
					<div className='winText'>Congratulations!</div>
					<div className='winText'>You won!</div>
					<button className='winButton' onClick={reset}>Play again</button>
				</div>}
				{
					data.map((row, rowIndex) => {
						return (
							<div style={{ display: 'flex' }} key={rowIndex}>
								{row.map((digit, index) => {
									return <Block number={digit} key={index} />;
								})}
							</div>
						);
					})
				}
			</div>
			<div className='gameinfo'>
				<b>HOW TO PLAY:</b> Use your <b>arrow keys</b> to move the tiles. Tiles with the same number merge into one when they touch. Add them up to reach <b>2048</b>!
			</div>
			<footer>Copyright © 2021 Robert Buritta</footer>
		</div>
	);
}

const Block = ({ number }) => {
	return <div className='block' style={{
		background: getColors(number),
		color: number === 2 || number === 4 ? '#645b52' : '#f7f4ef'
	}}>{number !== 0 ? number : ''}</div>;
};

const getColors = (number) => {
	switch (number) {
		case 2:
			return "#EBDCD0";
		case 4:
			return "#E9DBBA";
		case 8:
			return "#E9A067";
		case 16:
			return "#F08151";
		case 32:
			return "#F2654F";
		case 64:
			return "#F1462C";
		case 128:
			return "#E7C65E";
		case 256:
			return "#E8C350";
		case 512:
			return "#E8BE40";
		case 1024:
			return "#E8BB31";
		case 2048:
			return "#E7B723";
		default:
			return "#C2B3A3";
	}
};

export default App;
